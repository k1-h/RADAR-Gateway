package org.radarcns.gateway.auth

import org.radarcns.auth.authorization.Permission
import org.radarcns.auth.exception.TokenValidationException
import org.slf4j.LoggerFactory
import javax.annotation.Priority
import javax.ws.rs.Priorities
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.core.Context
import javax.ws.rs.core.Response
import javax.ws.rs.ext.Provider

/**
 * Authenticates user by a JWT in the bearer signed by the Management Portal.
 */
@Provider
@Authenticated
@Priority(Priorities.AUTHENTICATION)
class AuthenticationFilter : ContainerRequestFilter {

    @Context
    private lateinit var validator: AuthValidator

    override fun filter(requestContext: ContainerRequestContext) {
        val radarToken = try {
            validator.verify(requestContext)
        } catch (ex: TokenValidationException) {
            logger.warn("[401] {}: {}", requestContext.uriInfo.path, ex.message, ex)
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .header("WWW-Authenticate",
                                    BEARER_REALM
                                            + " error=\"invalid_token\""
                                            + " error_description=\"${ex.message}\"")
                            .build())
            null
        }

        if (radarToken == null) {
            logger.warn("[401] {}: No token bearer header provided in the request",
                    requestContext.uriInfo.path)
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                    .header("WWW-Authenticate", BEARER_REALM)
                    .build())
        } else if (!radarToken.hasPermission(Permission.MEASUREMENT_CREATE)) {
            val message = "MEASUREMENT.CREATE permission not given"
            logger.warn("[403] {}: {}", requestContext.uriInfo.path, message)
            requestContext.abortWith(
                    Response.status(Response.Status.FORBIDDEN)
                                .header("WWW-Authenticate", getInvalidScopeChallenge(message))
                            .build())
        } else {
            requestContext.securityContext = RadarSecurityContext(radarToken)
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(AuthenticationFilter::class.java)

        const val BEARER_REALM: String = "Bearer realm=\"Kafka REST Proxy\""
        const val BEARER = "Bearer "

        fun getInvalidScopeChallenge(message: String) = BEARER_REALM +
                " error=\"insufficient_scope\"" +
                " error_description=\"$message\"" +
                " scope=\"${Permission.MEASUREMENT_CREATE}\""
    }
}
